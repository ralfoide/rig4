package com.alflabs.rig4.exp;

import com.alflabs.annotations.NonNull;
import com.alflabs.rig4.EntryPoint;
import com.alflabs.rig4.HashStore;
import com.alflabs.rig4.flags.Flags;
import com.alflabs.rig4.gdoc.GDocHelper;
import com.alflabs.rig4.struct.GDocEntity;
import com.alflabs.rig4.struct.ArticleEntry;
import com.alflabs.utils.FileOps;
import com.alflabs.utils.ILogger;
import com.alflabs.utils.RPair;
import com.google.common.base.Charsets;
import org.jsoup.nodes.Element;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.alflabs.rig4.exp.ExpFlags.EXP_DEST_DIR;
import static com.alflabs.rig4.exp.ExpFlags.EXP_GA_UID;
import static com.alflabs.rig4.exp.ExpFlags.EXP_SITE_BANNER;
import static com.alflabs.rig4.exp.ExpFlags.EXP_SITE_BASE_URL;
import static com.alflabs.rig4.exp.ExpFlags.EXP_SITE_TITLE;

public class ArticleGenerator {
    private static final String TAG = ArticleGenerator.class.getSimpleName();

    private final Flags mFlags;
    private final ILogger mLogger;
    private final FileOps mFileOps;
    private final GDocHelper mGDocHelper;
    private final HashStore mHashStore;
    private final Templater mTemplater;
    private final HtmlTransformer mHtmlTransformer;

    @Inject
    public ArticleGenerator(
            Flags flags,
            ILogger logger,
            FileOps fileOps,
            GDocHelper gDocHelper,
            HashStore hashStore,
            Templater templater,
            HtmlTransformer htmlTransformer) {
        mFlags = flags;
        mLogger = logger;
        mFileOps = fileOps;
        mGDocHelper = gDocHelper;
        mHashStore = hashStore;
        mTemplater = templater;
        mHtmlTransformer = htmlTransformer;
    }

    void processEntries(@NonNull List<ArticleEntry> entries, boolean allChanged)
            throws IOException, URISyntaxException, InvocationTargetException, IllegalAccessException, ParseException {
        String destDir = mFlags.getString(EXP_DEST_DIR);
        mLogger.d(TAG, "        Site URL: " + mFlags.getString(EXP_SITE_BASE_URL));
        mLogger.d(TAG, "     Destination: " + destDir);

        for (ArticleEntry entry : entries) {
            String destName = entry.getDestName().replace('/', File.separatorChar);
            File destFile = new File(destDir, destName);

            mLogger.d(TAG, "Process document: " + destName);

            GDocEntity entity = mGDocHelper.getGDocAsync(entry.getFileId(), "text/html");
            String title = entity.getMetadata().getTitle();
            boolean keepExisting = true;
            String changed = "";
            if (allChanged) {
                keepExisting = false;
                changed = "all changed";
            } else if (!entity.isUpdateToDate()) {
                keepExisting = false;
                changed = "entity GDoc changed";
            } else if (!mFileOps.isFile(destFile)) {
                keepExisting = false;
                changed = "dest file missing";
            }

            String htmlHashKey = "html-hash-" + destFile.getPath();
            if (keepExisting) {
                String htmlHash = mHashStore.getString(htmlHashKey);
                keepExisting = htmlHash != null && htmlHash.equals(entity.getMetadata().getContentHash());
                if (!keepExisting) {
                    changed = "entity content changed";
                }
            }

            if (keepExisting) {
                mLogger.d(TAG, "   Keep existing: " + destName);
            } else {
                mLogger.d(TAG, "  Rebuild Reason: " + changed);
                RPair<Element, HtmlTransformer.LazyTransformer> intermediary = processHtml(entity.getContent(), title, destFile);
                entity.syncToStore();

                HtmlTransformer.LazyTransformer transformer = intermediary.second;
                Element transformed = transformer.lazyTransform(intermediary.first);
                String content = transformed.html();
                String relImageLink = transformer.getFormattedFirstImageSrc(transformed);
                // TODO for now there isn't a good way to generate *good* descriptions or get them from an article izu tag.
                // (The way I write my articles, the first extracted paragraph is the same as the title).
                String headDescription = null;

                String genInfo = "Generated on " + LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)
                        + " by Rig4j " + EntryPoint.getVersion();

                Templater.ArticleData data = new Templater.ArticleData(
                        mFlags.getString(EXP_SITE_TITLE),
                        mFlags.getString(EXP_SITE_BASE_URL),
                        "", // relative rev link is empty: articles are always at root of site URL.
                        "", // relative fwd link is empty: articles are always at root of site URL.
                        mFlags.getString(EXP_SITE_BANNER),
                        "", // css
                        mFlags.getString(EXP_GA_UID),
                        title,
                        destName,
                        content,
                        relImageLink,
                        headDescription,
                        genInfo);
                String html = mTemplater.generate(data);
                byte[] htmlContent = html.getBytes(Charsets.UTF_8);

                mFileOps.createParentDirs(destFile);
                mFileOps.writeBytes(htmlContent, destFile);
                mHashStore.putString(htmlHashKey, entity.getMetadata().getContentHash());
            }
        }
    }

    @NonNull
    private RPair<Element, HtmlTransformer.LazyTransformer> processHtml(@NonNull byte[] content, @NonNull String title, File destFile) throws IOException, URISyntaxException {
        HtmlTransformer.LazyTransformer transformer = mHtmlTransformer.createLazyTransformer(
                destFile.getPath(),
                new HtmlTransformer.Callback() {
                    @Override
                    public String processDrawing(String id, int width, int height, boolean useCache) throws IOException {
                        return mGDocHelper.downloadDrawing(id, destFile, width, height, useCache);
                    }

                    @Override
                    public String processImage(URI uri, int width, int height, boolean useCache) throws IOException {
                        return mGDocHelper.downloadImage(uri, destFile, width, height, useCache);
                    }
                });

        Element intermediary = mHtmlTransformer.simplifyForProcessing(content);
        return RPair.create(intermediary, transformer);
    }
}
